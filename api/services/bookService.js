const GOOGLE_API = 'https://www.googleapis.com/books/v1/';
const request = require("request");

module.exports = {
    import: (googleID) => {
        return new Promise( (success, reject) => {
            request.get( `${GOOGLE_API}volumes/${googleID}`, (e, r, b) => {
                if (r && r.statusCode && r.statusCode == 200 ){
                    let book = JSON.parse(b); 
                    book.volumeInfo.googleID = book.id;
                    book.volumeInfo.googleEtag = book.etag;

                    book.volumeInfo.industryIdentifiers.forEach( (i2) => {
                        if (i2 && i2.type && i2.identifier){
                            if (i2.type == "ISBN_10"){
                                book.volumeInfo.isbn10 = i2.identifier;
                            }
                
                            if (i2.type == "ISBN_13"){
                                book.volumeInfo.isbn13 = i2.identifier;
                            }
                        }
                    });
            
                    if (book.volumeInfo.googleID){
                        Book.count({googleID: book.volumeInfo.googleID}).exec( (err, count) => {
                            if (count < 1){       
                                
                                if (book.volumeInfo.authors && Array.isArray(book.volumeInfo.authors) ){
                                    book.volumeInfo.authors = book.volumeInfo.authors.toString();
                                }
            
                                if (book.volumeInfo.categories && Array.isArray(book.volumeInfo.categories) ){
                                    book.volumeInfo.categories = book.volumeInfo.categories.toString();
                                }
                                
                                if (book.volumeInfo.imageLinks && book.volumeInfo.imageLinks.thumbnail){
                                    book.volumeInfo.thumbnail = book.volumeInfo.imageLinks.thumbnail;
                                }
                                
                                if (book.volumeInfo.imageLinks && book.volumeInfo.imageLinks.smallThumbnail){
                                    book.volumeInfo.smallThumbnail = book.volumeInfo.imageLinks.smallThumbnail;
                                }
                                
                                Book.create(book.volumeInfo).exec( (err,created) =>{
                                    //console.log(err);
                                    return success(created.id);
                                });
                            }else{
                                // procurar o id ja cadastrado
                                Book.find({googleID: book.volumeInfo.googleID}).exec( (err, books) => {
                                    if (Array.isArray(books) && books[0] && books[0].id){
                                        return reject(books[0].id);
                                    }else{
                                        return reject(-1);
                                    }
                                });
                            }
                        });
                    }else{
                        return reject(-1);    
                    }
                }else{
                    return reject(-1);
                }
            });
        });
    },
    addUserBook: (user,book, library) => {
        UserBook.create({user: user, book:book, library:library}).exec( (err, ub) => {});
    },
    delUserBook: (user,book, library) => {
        UserBook.destroy({user: user, book:book, library:library}).exec( (err, ub) => {});
    }
}