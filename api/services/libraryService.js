module.exports = {
    load: (userID) => {
        return new Promise( (success, reject) => {
            Library.count({user: userID}).exec( (err, count) => {
                if (count < 1){
                    Library.create({user: userID}).exec( (err, u) => {
                        if (err){
                            return reject(err)
                        }else{
                            return success(u);
                        }
                    });
                }else{
                    Library.find({user: userID}).populateAll().exec( (err, users) => {
                        if (users && Array.isArray(users) && users[0] && users[0].id){
                            return success(users[0])
                        }else{
                            return reject(-1) 
                        }
                    });
                }
            });
        });        
    }
}