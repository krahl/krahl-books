module.exports = {
  attributes: {
    user: {model: 'user'},
    book: {model: 'book'},
    page: {type: 'string'},
    text: {type: 'string'}
  }
};