module.exports = {

  attributes: {
    title: {type:'string'},
    subtitle: {type:'string'},
    authors: {type:'string'},
    publisher: {type:'string'},
    publishedDate: {type:'string'},
    description: {type:'string'},
    isbn10: {type:'string'},
    isbn13: {type:'string'},
    pageCount: {type:'integer'},
    printType: {type:'string'},
    categories: {type:'string'},
    averageRating: {type:'integer'},
    ratingsCount: {type:'integer'},
    maturityRating: {type:'string'},
    thumbnail: {type:'string'},
    smallThumbnail: {type:'string'},
    language: {type:'string'},
    previewLink: {type:'string'},
    infoLink: {type:'string'},
    canonicalVolumeLink: {type:'string'},
    googleID: {type: 'string'},
    googleEtag: {type: 'string'},
    library: {collection: 'library', via: 'books'},
    wishlist: {collection: 'wishlist', via: 'books'}
  }
};