module.exports = {

  attributes: {
    userBook: {model: 'userBook'},
    pages: {type: 'integer'},
    duration: {type: 'integer'},//minutes
    date: {type: 'string'}
  }
};