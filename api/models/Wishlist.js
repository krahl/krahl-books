module.exports = {

  attributes: {
    user: {model: 'user', unique: true},
    books: {collection: 'book', via: 'wishlist', dominant: true}
  }
};