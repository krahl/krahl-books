module.exports = {

  attributes: {
    user: {model: 'user'},
    book: {model: 'book'},
    library: {model: 'library'},
    rating: {type: 'float'}
  }
};