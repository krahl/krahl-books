module.exports = {

  attributes: {
    user: {model: 'user', unique: true},
    books: {collection: 'book', via: 'library', dominant: true}
  }
};