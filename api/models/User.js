module.exports = {
  attributes: {
    email:{type: 'string',required: true, unique: true},
    password: { type: 'string', required: true },
    google: { type: 'string'},
    ativo: { type: 'boolean', defaultsTo: true },
    is_admin: { type: 'boolean', defaultsTo: false },
    recoverToken: { type: 'string'}
  }
};