let user = null;

module.exports = function (req, res, ok) {
    if (req.session.authenticated) {
        if(req.session.passport && req.session.passport.user) req.session.usuario = req.session.passport.user;
        return ok();
    } else {
        if(req.session.passport && req.session.passport.user){
            req.session.authenticated = true;
            return ok();
        }else{
            return res.redirect('/user/login');
        }
    }
}
