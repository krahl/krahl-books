module.exports = {
    index: (req,res) => {
        Quote.find({user: req.session.usuario.id}).populateAll().exec( (err, quotes) => {
            return res.view({quotes: quotes});
        });
    },
	edit: (req,res) => {
        let id = req.param("id");

        if (id){
            Quote.findOne(id).populateAll().exec( (err, q) => {
                if ( (q.user.id == req.session.usuario.id) && !err ){
                    return res.view({book: q.book, quote: q});
                }else{
                    return res.redirect('/quote');
                }
            });
        }else{
            let bookId = req.param("bookId");
            if (bookId){
                Book.findOne(bookId).exec( (err, book) => {
                    return res.view({book: book});
                });
            }else{
                return res.redirect('/quote');
            }
        }
    },
    update: (req,res) => {
        let id = req.param("id");
        let quote = {
            user: req.session.usuario.id,
            book: req.param("book"),
            page: req.param("page"),
            text: req.param("text")
        }

        if (quote.user && quote.book && quote.page && quote.text){
            if (id){
                Quote.findOne(id).populateAll().exec( (err, valid) => {
                    if (!err && valid.user.id == quote.user.id ){
                        Quote.update(id, quote).exec( (err, q) => {
                            return res.redirect('/quote');
                        });
                    }else{
                        return res.redirect('/quote');
                    }
                });
            }else{
                Quote.create(quote).exec( (err, q) => {
                    return res.redirect('/quote');
                });
            }
        }else{
            return res.redirect("/quote");
        }
    },
    destroy: (req,res) => {
        let id = req.param("id");

        if (id && id > 0){
            Quote.findOne(id).populateAll().exec( (err, valid) => {
                if (!err && valid && valid.user && valid.user.id == req.session.usuario.id){
                    Quote.destroy(id).exec( (err, q) => {
                        return res.redirect("/quote");
                    })
                }else{
                    return res.redirect("/quote");
                }
            })
        }else{
            return res.redirect("/quote");
        }
    }
};

