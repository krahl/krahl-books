const GOOGLE_API = 'https://www.googleapis.com/books/v1/';
const request = require("request");

module.exports = {
    search: (req,res) => {
        let search = req.param("search");
        
        if (!search || search.trim() == ""){
            return res.view({api: null, search: ""});
        }else{
            request.get( `${GOOGLE_API}volumes?q=${search}`, (e, r, b) => {
                if (r && r.statusCode && r.statusCode == 200 ){
                    return res.view({api: JSON.parse(b)}); 
                }else{
                    return res.view({api: null, search: search});
                }
            });
        }
    },
    import: (req,res) => {
        bookService.import(req.param('id')).then( (id) => {
            if (id < 1){
                return res.redirect('/book/search');
            }else{
                return res.redirect('/book/'+id);
            }
        }).catch( (id) => {
            if (id < 1){
                return res.redirect('/book/search');
            }else{
                return res.redirect('/book/'+id);
            }
        });
    },
    index: (req,res) => {
        Book.find().sort('title').exec( (err, books) => {
            return res.view({books: books});
        });
    },
    findOne: (req,res) => {
        let id = req.param("id");

        if (id){
            Book.findOne(id).exec( (err, book) => {
                if (book){
                    return res.view({book:book})
                }else{
                    return res.redirect('/book');
                }
            })
        }else{
            return res.redirect('/book');
        }
    }
};