function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

module.exports = {
    create: (req,res) => {
        let params = {
            name: req.param("name"),
            email: req.param("email"),
            password: req.param("password"),
            pass2: req.param("pass2")
        }
        if(params.name && params.email && params.pass2 == params.password && validateEmail(params.email)){
            User.count({email: params.email}).exec( (err, count) => {
                if (count > 0) return res.redirect('/user/login');
                User.create(params).exec( (err, user) => {
                    return res.redirect('/')
                });
            })
            //return res.redirect("/")
        }else{
            return res.redirect("/user/register");
        }
    },
    logar: (req,res) => {
        let email = req.param("email");
        let password = req.param("password");

        User.find().where({ email: email, ativo: true, password: password }).exec(function userFounded(err, user) {
            if (user && Object.keys(user).length > 0) {
                req.session.authenticated = true;
                req.user= user[0];
                //req.user.image = "/images/users/"+randomImage+".jpg";
                //req.user.nome = "Bem vindo!";
                req.session.usuario = user[0];
                //req.session.usuario.image = "/images/users/"+randomImage+".jpg";
                //req.session.usuario.nome = "Bem vindo!";

                //Load/Create User Library
                libraryService.load(user[0].id);
                return res.redirect('/');
            } else {
                flashService.createSessionFlash(req, 'Login Inválido!', 'error');
                return res.redirect('/user/login');
            }
        });
    },
    login: (req,res) => {
        return res.view({layout:''});
    },
    logout: (req, res) => {
        req.session.authenticated = false;
        req.user = null;
        req.session.usuario = null;
        return res.redirect("/user/login");
    },
    register: (req,res) => {
        return res.view({layout:''});
    },
    recoverpwd: (req,res) => {
        let email = req.param("email");

        User.find({email: email}).exec( (err, users) => {
            if (users && users[0] && users[0].email){
                const crypto = require("crypto");
                let recoverToken = crypto.randomBytes(20).toString('hex');
                User.update(users[0].id, {recoverToken: recoverToken}).exec( (err, u) => {
                    emailService.send(email, 'KrahlBooks - Recuperação de senha', 'http://krahl-books.herokuapp.com/user/recover/'+recoverToken)
                });
                return res.redirect("/user/login");
            }else{
                return res.redirect("/user/login");
            }
        })
    },
    recover: (req,res) => {
        let id = req.param('id');

        if (id && id != "" ){
            User.find({recoverToken: id}).exec( (err, users) => {
                if (users && users[0] && users[0].email){
                    return res.view({token: id, layout:''});
                }else{
                    return res.redirect('/user/login');        
                }
            })
        }else{
            return res.redirect('/user/login');
        }
    },
    updatePwd: (req,res) => {
        let token = req.param('token');
        let password = req.param('password');
        let password2 = req.param('password2');

        if (token && password && password == password2 && password != ""){
            User.find({recoverToken: token}).exec( (err, users) => {
                if (users && users[0] && users[0].email){
                    User.update(users[0].id, {password: password, recoverToken: ''}).exec( (err, u) => {

                    });
                    return res.redirect('/user/login');       
                }else{
                    return res.redirect('/user/login');        
                }
            })
        }else{
            return res.redirect('/user/login');
        }
    }
};

