module.exports = {
    index: (req,res) => {
        Thougth.find({user: req.session.usuario.id}).populateAll().exec( (err, thougths) => {
            return res.view({thougths: thougths});
        });
    },
	edit: (req,res) => {
        let id = req.param("id");

        if (id){
            Thougth.findOne(id).populateAll().exec( (err, q) => {
                if ( (q.user.id == req.session.usuario.id) && !err ){
                    return res.view({book: q.book, thougth: q});
                }else{
                    return res.redirect('/thougth');
                }
            });
        }else{
            let bookId = req.param("bookId");
            if (bookId){
                Book.findOne(bookId).exec( (err, book) => {
                    return res.view({book: book});
                });
            }else{
                return res.redirect('/thougth');
            }
        }
    },
    update: (req,res) => {
        let id = req.param("id");
        let thougth = {
            user: req.session.usuario.id,
            book: req.param("book"),
            page: req.param("page"),
            text: req.param("text")
        }

        if (thougth.user && thougth.book && thougth.page && thougth.text){
            if (id){
                Thougth.findOne(id).populateAll().exec( (err, valid) => {
                    if (!err && valid.user.id == thougth.user.id ){
                        Thougth.update(id, thougth).exec( (err, q) => {
                            return res.redirect('/thougth');
                        });
                    }else{
                        return res.redirect('/thougth');
                    }
                });
            }else{
                Thougth.create(thougth).exec( (err, q) => {
                    return res.redirect('/thougth');
                });
            }
        }else{
            return res.redirect("/thougth");
        }
    },
    destroy: (req,res) => {
        let id = req.param("id");

        if (id && id > 0){
            Thougth.findOne(id).populateAll().exec( (err, valid) => {
                if (!err && valid && valid.user && valid.user.id == req.session.usuario.id){
                    Thougth.destroy(id).exec( (err, q) => {
                        return res.redirect("/thougth");
                    })
                }else{
                    return res.redirect("/thougth");
                }
            })
        }else{
            return res.redirect("/thougth");
        }
    }
};

