var passport = require('passport');

module.exports = {
    google: function (req, res) {
        passport.authenticate('google', { scope: ['profile', 'email'] }, function (err, user, info) {
            if ((err) || (!user)) {
                //flashService.createSessionFlash(req, "Dados inválidos!", 'error');
                return res.send({
                    message: info.message,
                    user: user,
                    text: 'failed'
                });
            }
            console.log("authenticated")
            req.session.authenticated = true;
            req.session.usuario = user;
            req.login(user, function (err) {
                if (err) {
                    return res.send(err);
                }
                
                return res.send({
                    message: info.message,
                    user: user
                });
            });
        })(req, res);
    },
    callback: function (req, res, next) {
        passport.authenticate('google', {
            successRedirect: '/',
            failureRedirect: '/user/login'
        })(req, res, next);
    }
};
