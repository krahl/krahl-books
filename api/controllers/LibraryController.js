module.exports = {
    addBook: (req,res) => {
        let bookID = req.param("book");

        if (bookID){
            Book.findOne(bookID).exec( (err, adBook) => {
                if (adBook && adBook.id){
                    libraryService.load(req.session.usuario.id).then( (lib) => {
                        let libBooks = sails.util.pluck( lib.books, 'id');

                        if (libBooks.indexOf(bookID) == -1){
                            libBooks.push(bookID);
                            Library.update(lib.id, {books: libBooks}).exec( (err, libU) => {
                                bookService.addUserBook(req.session.usuario.id, bookID, lib.id);
                                return res.redirect('/library/book');
                            });
                        }else{
                            return res.redirect('/library/book');
                        }
                    }).catch( (err) => {
                        return res.redirect("/library");    
                    });
                }else{
                    return res.redirect("/library");
                }
            });
        }else{
            return res.redirect("/library");
        }
    },
    removeBook: (req,res) => {
        let bookID = req.param("book");
        if (bookID){
            libraryService.load(req.session.usuario.id).then( (lib) => {
                let libBooks = sails.util.pluck( lib.books, 'id');
                if (libBooks.indexOf(parseInt(bookID)) != -1){
                    libBooks.splice(libBooks.indexOf(parseInt(bookID)), 1);
                    Library.update(lib.id, {books: libBooks}).exec( (err, libU) => {
                        bookService.delUserBook(req.session.usuario.id, bookID, lib.id);
                        return res.redirect('/library/book');
                    });
                }else{
                    return res.redirect('/library/book');
                }
            }).catch( (err) => {
                return res.redirect('/library/book');
            });
        }else{
            return res.redirect('/library/book');
        }
    },
    book: (req,res) => {
        libraryService.load(req.session.usuario.id).then( (lib) => {
            if (lib && lib.books){
                return res.view({library: lib});
            }else{
                return res.redirect('/book/search');
            }   
        });
    },
    info: (req,res) => {
        let bookID = req.param("book");
        if (bookID){
            libraryService.load(req.session.usuario.id).then( (lib) => {
                if (lib && lib.books){
                    UserBook.find({book: bookID, user: req.session.usuario.id, library: lib.id}).populateAll().exec( (err, ub) => {
                        if (ub && ub[0] && ub[0].id){
                            UserBookSession.find({userBook:ub[0].id}).exec( (err, sessions) => {
                                return res.view({userBook: ub[0], sessions: sessions});
                            });
                            
                        }else{
                            bookService.addUserBook(req.session.usuario.id, bookID, lib.id);
                            return res.redirect('/library/book');
                        }
                    });
                }else{
                    return res.redirect('/library/book');
                }   
            });
        }else{
            return res.redirect('/library/book');
        }
    },
    addsession: (req,res) => {
        let userBookID = req.param("userBook");
        let params = {
            pages: req.param("pages"),
            duration: req.param("duration"),
            date: req.param("date")
        }
        if (userBookID && params.pages && params.duration && params.date){
            UserBook.findOne(userBookID).populateAll().exec( (err, userBook) => {
                if (!err && userBook && userBook.user.id == req.session.usuario.id){
                    UserBookSession.create({userBook: userBookID, pages: params.pages, duration: params.duration, date: params.date}).exec( (err, session) => {
                        return res.redirect(`/library/info?book=${userBook.book.id}`);
                    });
                }else{
                    return res.redirect(`/library/book`);      
                }
            })
        }else{
            return res.redirect('/library/book');
        }
    }
};

