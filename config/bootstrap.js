/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {
  /*var isbn = require('node-isbn');

  isbn.resolve('0000000000000', function (err, book) {
      if (err) {
          console.log('Book not found', err);
      } else {
        Book.create(book, (err, createdBook) => {
          console.log('Book found %j', book);
        });          
      }
  });*/
  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
