const passport = require('passport'),
    GoogleStrategy = require('passport-google-oauth20').Strategy;

    // Take user object, store information in a session
passport.serializeUser(function (user, done) {
    console.log(user);
    done(null, user);
});

// Take information from the session, check if the session is still valid
passport.deserializeUser(function (userP, done) {
    User.findById(userP.id, function (err, user) {
        done(err, user);
    });
});

passport.use(new GoogleStrategy({
    // Make sure you add the Google+ API to your application
    clientID: "1033405122115-obpr602klj8kv6b9dh2si4d3d7r17ote.apps.googleusercontent.com",
    clientSecret: "rukAAUFBPOn4ZMBFUAu3KCbY",
    // Google doesn't allow the use of localhost in the callbackURL
    callbackURL: "/auth/callback"
},
function (accessToken, refreshToken, profile, done) {
    console.log(profile);
    User.count({email: profile.emails[0].value}).exec( (err, count) => {
        if(count < 1){
            let photo = null;
            if (profile.photos){
                if (profile.photos[0]){
                    if (profile.photos[0].value){
                        photo = profile.photos[0].value;
                    }
                }
            }
            
            var returnUser = {
                email: profile.emails[0].value,
                image: photo,
                nome: profile.displayName,
                password: "B@&R!@GRHd829duhi8d!9dwqd",
                google: profile.id
            }
            User.create(returnUser).exec( (err, user) => {
                return done(null, returnUser, { message: 'User Created!'});
            });
            
        }else{
            User.findOne({ email: profile.emails[0].value, ativo: true }, function (err, user) {
                if (err){
                    return done(err);
                } else{
                    // No user found, then create an account
                    if (!user || user == 'false' ) {
                        return done(err, false, { message: 'Something went wrong' });
                    } else {
                        User.update({ email: user.email }, { google: profile.id }, function (err, user) {
                            if (err) {
                                return done(err, false, { message: 'Something went wrong' });
                            } else {
                                let photo = null;
                                if (profile.photos){
                                    if (profile.photos[0]){
                                        if (profile.photos[0].value){
                                            photo = profile.photos[0].value;
                                        }
                                    }
                                }
                                
                                var returnUser = {
                                    email: user[0].email,
                                    createdAt: user[0].createdAt,
                                    id: user[0].id,
                                    image: photo,
                                    nome: profile.displayName
                                }
                                return done(null, returnUser, { message: 'User already registered, linked Google account' });
                            }
                        })
                    }
                }
            });
        }
    });
}));
