module.exports.policies = {
  '*': ['flash','isAuthenticated'],
  'user': {
    'login': ['flash'],
    'register': ['flash'],
    'create': ['flash'],
    'logar': ['flash'],
    'logout': ['flash'],
    'recoverpwd': ['flash'],
    'recover': ['flash'],
    'updatePwd': ['flash'],
    'index':['flash','isAuthenticated','isAdmin'],
    'update':['flash','isAuthenticated','isAdmin'],
    'edit':['flash','isAuthenticated','isAdmin'],
    'destroy':['flash','isAuthenticated','isAdmin']
  },
  'auth': []
};
