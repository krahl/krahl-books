FROM node:latest
LABEL Author="Lucas Krahl<luckrahl1996@gmail.com>"
USER root
WORKDIR /var/www
COPY ./ /var/www
RUN cd /var/www && npm i -g sails@0.12.14
RUN npm install
EXPOSE 1337
CMD ["sails", "lift"]